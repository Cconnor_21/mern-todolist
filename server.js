const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const path = require('path');

const items = require('./routes/api/items');

const app = express();

//Body parser middle ware
app.use(bodyParser.json());

//db config
const db = require('./config/keys').mongoURI;

//connect to mongodb
mongoose.connect(db)
  .then(() => console.log('Mongo db connected...'))
  .catch(err => console.log(err));

//use routes
app.use('/api/items', items);

//require static production build folder
app.use(express.static(path.join(__dirname, "client/build")));

//use static assets if in production
//if(process.env.NODE_ENV === 'production'){
  //set static folder
  //console.log("this is the current node env" + process.env.NODE_ENV);
  //app.use(express.static('client/build'));
  //app.get('*', (req, res) => {
    //res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  //});
//}


const port = 8080;

app.listen(port, () => console.log(`server started on port ${port}`));


/*
//Mysql Backend setup
const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'TodoList'
});

const app = express();
app.use(bodyParser.json());

app.get('/items', (req, res) => {
    connection.query('SELECT name FROM todos', (error, results, fields) => {
      if (error) throw error;
      res.send(JSON.stringify(results))
    });
});

app.post('/items', (req, res) => {
  connection.query(`insert into todos(name) values('${req.body.name}')`, (error, results) => {
    if(error) throw error;
    res.send(item => JSON.stringify(results));
  });
});

app.delete('/items:id', (req, res) => {
  connection.query(`delete from todos where _id = ${req.body._id}`, (error, results) => {
    if(error) throw error;
  })
})

const port = process.env.PORT || 5000;
app.listen(port, () => {
 console.log(`Go to http://localhost:${port}/items to see items`);
});*/
