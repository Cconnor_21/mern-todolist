import React, {Component} from 'react';
import AppNavbar from './components/AppNavbar';
import 'bootstrap/dist/css/bootstrap.min.css';
import TodoList from './components/TodoList';
import ItemModal from './components/ItemModal';
import {Container} from 'reactstrap';
import './App.css';

import {Provider} from 'react-redux';
import store from './store';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
      <div>
        <AppNavbar></AppNavbar>
        <Container>
        <ItemModal />
        <TodoList />
        </Container>
      </div>
      </Provider>
    );
  }
}

export default App;
